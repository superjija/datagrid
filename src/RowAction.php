<?php

namespace Jija\Datagrid;

use Nette;

class RowAction extends \Nette\Object
{

    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $link;
    /**
     * @var string
     */
    private $icon;
    /**
     * @var string
     */
    private $cssClass;
    /**
     * @var string
     */
    private $confirmQuestion;
    /**
     * @var string
     */
    private $ownRender="";

    /**
     * @var boolean
     */
    private $isAjax;

    public function __construct($name, $title, $link, $icon, $isAjax)
    {
        $this->name = $name;
        $this->title = $title;
        $this->link = $link;
        $this->icon = $icon;
        $this->isAjax = $isAjax;
    }

    /**
     * @return boolean
     */
    public function getIsAjax()
    {
        return $this->isAjax;
    }

    /**
     * @param boolean $isAjax
     * @return RowAction
     */
    public function setIsAjax($isAjax)
    {
        $this->isAjax = $isAjax;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return RowAction
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return RowAction
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return RowAction
     */
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     * @return RowAction
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @return string
     */
    public function getOwnRender()
    {
        return $this->ownRender;
    }

    /**
     * @param string $ownRender
     * @return RowAction
     */
    public function setOwnRender($ownRender)
    {
        $this->ownRender = $ownRender;
        return $this;
    }

    /**
     * @return string
     */
    public function getCssClass()
    {
        return $this->cssClass;
    }

    /**
     * @param string $cssClass
     * @return RowAction
     */
    public function setCssClass($cssClass)
    {
        $this->cssClass = $cssClass;
        return $this;
    }

    /**
     * @return string
     */
    public function getConfirmQuestion()
    {
        return $this->confirmQuestion;
    }

    /**
     * @param string $confirmQuestion
     * @return RowAction
     */
    public function setConfirmQuestion($confirmQuestion)
    {
        $this->confirmQuestion = $confirmQuestion;
        return $this;
    }


}
