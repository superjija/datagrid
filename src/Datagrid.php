<?php

namespace Jija\Datagrid;

use Jija\Datagrid\Datasources\DoctrineQBDataSource;
use Nette;
use Nette\Application\Responses\JsonResponse;
use Nette\Application\UI;

class Datagrid extends UI\Control
{

    /** @var array */
    protected $columns = [];

    /** @var array */
    protected $rowActions = [];

    /** @var array */
    protected $gridActions = [];

    /** @var string */
    protected $rowPrimaryKey;
    /** @var string */
    protected $elementId;

    /** @var mixed */
    protected $data;
    private $templateHelperLoaders = [];
    protected $templateParameters = [];
    protected $defaultSort;
    protected $defaultItemsCount;
    protected $filteredDataCount;
    protected $showHeaderFilters = FALSE;
    protected $showFooterFilters = FALSE;
    protected $showRowCheckboxes = FALSE;
    protected $filterable = TRUE;
    protected $paginable = TRUE;
    protected $dataSource;
    private $languagesPath;
    private $language='cs';

    public function __construct(
        Nette\ComponentModel\IContainer $parent = NULL,
        $name = ''
    ) {
        parent::__construct($parent,$name);
        $this->defaultItemsCount = 10;
        $this->defaultSort = '[1,"asc"]';
        $this->elementId = 'datagrid_' . rand(1, 1000);
        $this->dataSource = new DoctrineQBDataSource();
        $this->languagesPath = $parent->template->basePath . '/assets/js/datagrid/lang/';
    }

    public function getConfig()
    {
        $config = [];
        $config['element'] = '.dg_' . $this->elementId;
        $lang =$this->getLanguageUrl( $this->language);
        if ($lang) {
            $config['languageUrl'] = $lang;
        }

        $config['dataSourceUrl'] = $this->link("dataJSON!");
        $config['updateDataUrl'] = $this->link("updateData!");
        $config['pageLength'] = $this->defaultItemsCount;
        $config['defaultSort'] = $this->defaultSort;
        $config['filterable'] = $this->filterable;
        $config['paginable'] = $this->paginable;
        $config['paginationType'] = 'simple_numbers';
        $columns = [];
        $mappers = [];
        $rowActions = [];
        foreach ($this->columns as $column) {
            $col = new \stdClass;
            if (!empty($column->getJsMapper())) {
                $mappers[$column->getName()] = $column->getJsMapper();
                $col->jsMapper = $column->getJsMapper();
            }
            $col->name = $column->getName();
            $col->type = $column->getType();
            $col->customRender = $column->getCustomRender();
            $col->editable = $column->isEditable();
            $col->editableType = $column->getEditableType();
            $columns[] = $col;
        }
        $config['columns'] = $columns;
        $config['mappers'] = $mappers;

        foreach ($this->rowActions as $rowAction) {
            $row = new \stdClass;
            $row->isAjax = $rowAction->getIsAjax();
            $row->link = $this->presenter->link($rowAction->getLink(), "#");
            $row->icon = $rowAction->getIcon();
            $row->title = $rowAction->getTitle();
            $row->class = $rowAction->getCssClass();
            $row->ownRender = $rowAction->getOwnRender();
            $row->confirmQuestion = $rowAction->getConfirmQuestion();
            $rowActions[] = $row;
        }
        $config['rowActions'] = $rowActions;
        $config['hasRowActions'] = count($this->rowActions) > 0;
        return $config;
    }

    public function addTemplateHelperLoader($loader)
    {
        $this->templateHelperLoaders[] = $loader;
        return $this;
    }

    /**
     * Adds column
     * @param  string
     * @param  string
     * @return Column
     */
    public function addColumn($name, $label = NULL, $toolTip = NULL)
    {
        if (!$this->rowPrimaryKey) {
            $this->rowPrimaryKey = $name;
        }

        $label = $label ? $label : ucfirst($name);
        return $this->columns[$name] = new Column($name, $label, NULL, $toolTip);
    }

    /**
     * Adds row action
     * @param  string
     * @param  string
     * @return RowAction
     */
    public function addRowAction($name, $title, $link, $icon, $isAjax)
    {
        $action = new RowAction($name, $title, $link, $icon, $isAjax);
        $this->rowActions[$name] = $action;
        return $action;
    }

    /**
     * Adds row action
     * @param  string
     * @param  string
     * @return RowAction
     */
    public function addGridAction($name, $title, $link, $icon, $isAjax = FALSE)
    {
        $action = new GridAction($name, $title, $link, $icon, $isAjax = FALSE);
        $this->gridActions[$name] = $action;
        return $action;
    }

    public function removeColumn($name)
    {
        unset($this->columns[$name]);
    }

    /**
     *
     * @param type $name
     * @param type $label
     * @return DateTimeColumn
     */
    public function addDateTimeColumn($name, $label = NULL)
    {
        $label = $label ? $label : ucfirst($name);
        return $this->columns[$name] = new DateTimeColumn($name, $label, NULL);
    }

    public function render()
    {
        $this->template->columns = $this->columns;
        $this->template->gridActions = $this->gridActions;
        $this->template->rowActions = $this->rowActions;
        $this->template->rowPrimaryKey = $this->rowPrimaryKey;
        $this->template->defaultItemsCount = $this->defaultItemsCount;
        $this->template->showHeaderFilters = $this->showHeaderFilters;
        $this->template->showFooterFilters = $this->showFooterFilters;
        $this->template->showRowCheckboxes = $this->showRowCheckboxes;
        $this->template->filterable = $this->filterable;
        $this->template->defaultSort = json_encode($this->defaultSort);

        $this->template->id = $this->elementId;


        foreach ($this->templateParameters as $key => $param) {
            $this->template->{$key} = $param;
        }
        $this->template->setFile(__DIR__ . '/datagrid.latte');

        // template helpers
        foreach ($this->templateHelperLoaders as $loader) {
            $this->template->registerHelperLoader($loader);
        }
        $this->template->render();
    }

    public function handleDataJSON()
    {
        $request = $this->presenter->context->getByType('Nette\Http\Request');
        $limit = $request->getQuery("length");
        $start = $request->getQuery("start");
        $draw = $request->getQuery("draw");
        $filters = $this->getFilters($request);
        if ($request->getQuery("order") != NULL) {

            $orderColumn = $request->getQuery("columns")[$request->getQuery("order")[0]["column"]]["name"];
            $orderDir = $request->getQuery("order")[0]["dir"];
        } else {
            $orderColumn = NULL;
            $orderDir = NULL;
        }
        $data = $this->getData($start, $limit, $orderColumn, $orderDir, $filters);
        $response = new \stdClass();
        $response->draw = $draw;
        $response->recordsTotal = $this->getDataCount();
        $response->recordsFiltered = $this->filteredDataCount;
        $response->data = $data;
        $this->presenter->sendResponse(new JsonResponse($response));
    }

    public function handleupdateData()
    {
        $request = $this->presenter->context->getByType('Nette\Http\Request');
        $id = $request->getPost("id");
        $column = $request->getPost("columnName");
        $value = $request->getPost("value");
        $response = $this->dataSource->updateData($column, $id, $value);
        $this->presenter->sendResponse(new \Nette\Application\Responses\TextResponse($response));
    }

    public function setRowPrimaryKey($columnName)
    {
        $this->rowPrimaryKey = (string)$columnName;
    }

    public function getRowPrimaryKey()
    {
        return $this->rowPrimaryKey;
    }


    protected function getData($start = 0, $limit = 0, $orderColumn = NULL, $orderDir = "ASC", $filters = [])
    {
        $data = $this->dataSource->getData($filters, $orderColumn, $orderDir, $limit, $start);
        $this->filteredDataCount = $data["count"];
        $this->data = [];
        $data = $data["data"];
        $specialColumns = [];
        foreach ($this->columns as $column) {
            if ($column->getFormatCallback() !== NULL) {
                $specialColumns[] = $column;
            }
        }
        foreach ($data as $row) {
            if (is_array($row)) {
                $row["DT_RowId"] = $row["id"];
                foreach ($specialColumns as $column) {
                    $row[$column->name] = $column->format($row[$column->name], $row);
                }
                $this->data[] = (object)$row;
            }
        }
        return $this->data;
    }

    protected function getFilters($request)
    {
        $filters = [];
        $query = $request->getQuery("search")["value"];

        foreach (explode("&&", $query) as $filter) {
            $parts = explode("||", $filter);
            if (count($parts) === 2) {
                $filters[$parts[0]] = $parts[1];
            }
        }
        return $filters;
    }

    protected function getDataCount()
    {
        $count = $this->dataSource->getDataCount();
        return $count;
    }

    protected function createTemplate($class = NULL)
    {
        $template = parent::createTemplate($class);
        return $template;
    }

    public function getDefaultSort()
    {
        return $this->defaultSort;
    }

    public function setDefaultSort($defaultSort)
    {
        $this->defaultSort = $defaultSort;
        return $this;
    }

    public function setShowHeaderFilters($showHeaderFilters = TRUE)
    {
        $this->showHeaderFilters = $showHeaderFilters;
        return $this;
    }


    public function setShowFooterFilters($showFooterFilters = TRUE)
    {
        $this->showFooterFilters = $showFooterFilters;
        return $this;
    }

    public function getDefaultItemsCount()
    {
        return $this->defaultItemsCount;
    }

    public function setDefaultItemsCount($defaultItemsCount)
    {
        $this->defaultItemsCount = $defaultItemsCount;
        return $this;
    }

    public function getTrueFalseMapper()
    {
        return [
            "1" => "Ano",
            "0" => "Ne",
            "true" => "Ano",
            "false" => "Ne",
        ];
    }

    /**
     * @return boolean
     */
    public function isFilterable()
    {
        return $this->filterable;
    }

    /**
     * @param boolean $filterable
     * @return Datagrid
     */
    public function setFilterable($filterable)
    {
        $this->filterable = $filterable;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isPaginable()
    {
        return $this->paginable;
    }

    /**
     * @param boolean $paginable
     * @return Datagrid
     */
    public function setPaginable($paginable)
    {
        $this->paginable = $paginable;
        return $this;
    }



    private function getLanguageUrl($language)
    {
        $lang = NULL;
        switch ($language) {
            case("cs"):
                $lang = $this->languagesPath . 'lang/Czech.json';
                break;
            case("de"):
                $lang = $this->languagesPath . 'lang/German.json';
                break;
            case("hu"):
                $lang = $this->languagesPath . 'lang/Hungarian.json';
                break;
            case("pl"):
                $lang = $this->languagesPath . 'lang/Polish.json';
                break;
            case("ro"):
                $lang = $this->languagesPath . 'lang/Romanian.json';
                break;
            case("ru"):
                $lang = $this->languagesPath . 'lang/Russian.json';
                break;
            case("se"):
                $lang = $this->languagesPath . 'lang/Swedish.json';
                break;
            case("sk"):
                $lang = $this->languagesPath . 'lang/Slovak.json';
                break;
            default:
                $lang = $this->languagesPath . 'lang/English.json';
                break;
        }
        return $lang;
    }

    /**
     * @param DoctrineQBDataSource $dataSource
     * @return Datagrid
     */
    public function setDataSource($dataSource)
    {
        $this->dataSource = $dataSource;
        return $this;
    }

    /**
     * @return DoctrineQBDataSource
     */
    public function getDataSource()
    {
        return $this->dataSource;
    }

    /**
     * @return boolean
     */
    public function isShowRowCheckboxes()
    {
        return $this->showRowCheckboxes;
    }

    /**
     * @param boolean $showRowCheckboxes
     * @return Datagrid
     */
    public function setShowRowCheckboxes($showRowCheckboxes)
    {
        $this->showRowCheckboxes = $showRowCheckboxes;
        return $this;
    }

    /**
     * @param string $languagesPath
     * @return Datagrid
     */
    public function setLanguagesPath($languagesPath)
    {
        $this->languagesPath = $languagesPath;
        return $this;
    }



}
