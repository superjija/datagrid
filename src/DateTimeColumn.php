<?php

namespace Jijas\Datagrid;



class DateTimeColumn extends Column {

    public function __construct( $name, $label, $toolTip = NULL ) {
        parent::__construct($name, $label, $toolTip = NULL);
        $this->type = null;
        $this->setFormatCallback($this->format);
    }

    private $dateFormat = 'd.m.Y';

    public function setFormat( $format ) {
        $this->dateFormat = $format;
        return $this;
    }

    public function getFormat() {
        return $this->dateFormat;
    }

    public function format( $cell, $row ) {
        if ($cell instanceof \DateTime) {
            $cell = $cell->format($this->dateFormat);
        }
        return $cell;
    }

}
