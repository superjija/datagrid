<?php

namespace Jija\Datagrid;

use Nette;

class Column extends Nette\Object {

    const ALIGN_LEFT = 'left';
    const ALIGN_CENTER = 'center';
    const ALIGN_RIGHT = 'right';

    /** @var string */
    protected $align = self::ALIGN_LEFT;

    /** @var string */
    protected $name;

    /** @var string */
    protected $label;

    /** @var string */
    protected $style;

    /** @var string */
    protected $toolTip;

    /** @var string */
    protected $sort = FALSE;

    /** @var boolean */
    protected $escapeCellHtml = FALSE;

    /** @var string */
    protected $type;

    /** @var boolean */
    protected $filterable=true;
    /** @var boolean */
    protected $editable=false;

    /** @var string */
    protected $editableType;
    /** @var string */
    protected $customRender=false;

    /** @var array */
    protected $jsMapper=null;
    protected $nullValue = "";
    protected $formatCallback;

    public function __construct( $name, $label, $toolTip = NULL ) {
        $this->name = $name;
        $this->label = $label;
        $this->toolTip = $toolTip;
        $this->editable = null;
        $this->enableSort();
    }

    public function escapeCellHtml() {
        return $this->escapeCellHtml;
    }

    public function setEscapeCellHtml( $escapeCellHtml ) {
        $this->escapeCellHtml = $escapeCellHtml;
        return $this;
    }

    public function setNullValue( $nullValue ) {
        $this->nullValue = $nullValue;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isFilterable()
    {
        return $this->filterable;
    }

    public function setFilterable( $filterable ) {
        $this->filterable = $filterable;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isEditable()
    {
        return $this->editable;
    }


    public function setEditable( $editable = "" ) {
        if ($editable === FALSE) {
            $this->editable = FALSE;
        }
        else {
            $this->editable = TRUE;
            $this->editableType = $editable;
        }
        return $this;
    }

    public function setFormatCallback( $callback ) {
        $this->formatCallback = $callback;
        return $this;
    }
    public function getFormatCallback(  ) {
        return $this->formatCallback ;
    }

    public function alignLeft() {
        return $this->align(self::ALIGN_LEFT);
    }

    public function alignRight() {
        return $this->align(self::ALIGN_RIGHT);
    }

    public function alignCenter() {
        return $this->align(self::ALIGN_CENTER);
    }

    public function align( $align ) {
        $this->align = $align;
        return $this;
    }

    public function enableSort( $state = TRUE ) {
        $this->sort = $state;
        return $this;
    }

    public function canSort() {
        return $this->sort;
    }

 


    public function format( $cell, $row ) {
        if ($this->formatCallback) {
            $callback=callback($this->formatCallback);
            $cell = $callback->invokeArgs(array($cell, $row));
        }
        return $cell;
    }

    public function setJSMapper( $mapper ) {
        $this->jsMapper = $mapper;
        return $this;
    }
    public function getJSMapper(  ) {
        return $this->jsMapper;
    }
    public function getJSMapperJSON(  ) {
        return json_encode($this->jsMapper);
    }

    public function setStyle( $style ) {
        $this->style = $style;
    }

    public function setType( $type ) {
        $this->type = $type;
        return $this;
    }
    public function setCustomRender( $render ) {
        $this->setType("custom");
        $this->customRender = $render;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomRender()
    {
        return $this->customRender;
    }

    public function getType(  ) {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Column
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getToolTip()
    {
        return $this->toolTip;
    }

    /**
     * @param string $toolTip
     * @return Column
     */
    public function setToolTip($toolTip)
    {
        $this->toolTip = $toolTip;
        return $this;
    }

    /**
     * @return string
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param string $sort
     * @return Column
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
        return $this;
    }

    /**
     * @return string
     */
    public function getEditableType()
    {
        return $this->editableType;
    }

    /**
     * @param string $editableType
     * @return Column
     */
    public function setEditableType($editableType)
    {
        $this->editableType = $editableType;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return Column
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }


}
