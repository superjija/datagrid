<?php
namespace Jija\Datagrid\Datasources;


class DoctrineQBDataSource implements IDatagridDataSource
{
    /** @var \Nette\Utils\Callback */
    protected $dataSourceCallback;

    /** @var \Nette\Utils\Callback */
    protected $dataUpdateCallback;

    /** @var \Nette\Utils\Callback */
    protected $dataCountCallback;

    public function getData($filters = [], $orderColumn = NULL, $orderDir = "ASC", $limit = NULL, $start = 0)
    {
        $query = \callback($this->dataSourceCallback);
        $query = $query->invoke();
        $selectParts = $query->getDQLParts()["select"][0]->getParts();
        if ($orderColumn != NULL) {
            foreach ($selectParts as $column) {
                if (\Nette\Utils\Strings::contains($column, $orderColumn)) {
                    $query->orderBy(explode(' ', trim($column))[0], $orderDir);
                    if (!\Nette\Utils\Strings::contains($column, $orderColumn . " ")) { // neobsahuje alias
                        break;
                    }
                }
            }
        }
        foreach ($filters as $col => $value) {
            foreach ($selectParts as $column) {
                $parts = explode(' ', trim($column)); // column and alias
                if (\Nette\Utils\Strings::contains($parts[count($parts) - 1], $col)) {
                    $query->andWhere($parts[0] . " LIKE :p" . $col);
                    $query->setParameter(":p" . $col, "%" . $value . "%");
                }
            }
        }

        $data = $query->getQuery()->getResult();
        $count=count($data);
        if ($limit > 0) {
            $query->setMaxResults($limit)->setFirstResult($start);
        }
        $data = $query->getQuery()->getArrayResult();
        return ["data"=>$data,"count"=>$count];
    }

    public function getDataCount()
    {
        $callback = callback($this->dataCountCallback);
        $data = $callback->invoke()->getQuery()->getArrayResult();
        return (int)$data[0]["1"];
    }

    public function updateData($column, $id, $value)
    {
        // TODO: Implement updateData() method.
    }


    /**
     * @param \Nette\Utils\Callback $dataSourceCallback
     * @return DoctrineQBDataSource
     */
    public function setDataSourceCallback($dataSourceCallback)
    {
        $this->dataSourceCallback = $dataSourceCallback;
        return $this;
    }

    /**
     * @param \Nette\Utils\Callback $dataUpdateCallback
     * @return DoctrineQBDataSource
     */
    public function setDataUpdateCallback($dataUpdateCallback)
    {
        $this->dataUpdateCallback = $dataUpdateCallback;
        return $this;
    }

    /**
     * @param \Nette\Utils\Callback $dataCountCallback
     * @return DoctrineQBDataSource
     */
    public function setDataCountCallback($dataCountCallback)
    {
        $this->dataCountCallback = $dataCountCallback;
        return $this;
    }


}