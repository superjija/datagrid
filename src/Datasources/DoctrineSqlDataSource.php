<?php
/**
 * Created by PhpStorm.
 * User: jija
 * Date: 30.11.2015
 * Time: 17:12
 */

namespace Jija\Datagrid\Datasources;


use Doctrine\ORM\EntityManager;
use Mistylab\Doctrine\SQLLoggerBroker;

class DoctrineSqlDataSource implements IDatagridDataSource
{
    /** @var \Nette\Utils\Callback */
    protected $dataSourceCallback;

    /** @var \Nette\Utils\Callback */
    protected $dataUpdateCallback;

    /** @var \Nette\Utils\Callback */
    protected $dataCountCallback;

    protected $defaultFilters = [];

    protected $defaultOrders = [];


    /** @var EntityManager */
    protected $em;

    /**
     * DoctrineSqlDataSource constructor.
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    public function getData($filters = [], $orderColumn = NULL, $orderDir = 'ASC', $limit = NULL, $start = 0)
    {
        $method = \callback($this->dataSourceCallback);
        $sql = $method->invoke();


        $where = '';
        $params = [];
        foreach ($this->defaultFilters as $filter) {
            if (strlen($where) > 0) {
                $where .= ' AND ';
            }
            $where .= $filter['column'] . ' ' . $filter['operator'] . ' :p' . $filter['column'];
            $params['p' . $filter['column']] = $filter['value'];
        }
        foreach ($filters as $col => $value) {
            if (strlen($where) > 0) {
                $where .= ' AND ';
            }
            $where .= $col . ' LIKE :p' . $col;
            $params['p' . $col] = '%' . $value . '%';
        }
        if (strlen($where) > 0) {
            $sql .= ' WHERE ' . $where;
        }

        $order = '';
        foreach ($this->defaultOrders as $sort) {
            if (strlen($order) > 0) {
                $order .= ' , ';
            }
            $order .= $sort['column'] . ' ' . $sort['direction'];
        }
        if ($orderColumn != NULL) {
            if (strlen($order) > 0) {
                $order .= ' , ';
            }
            $order .= $orderColumn . " " . $orderDir;
        }
        if (strlen($order) > 0) {
            $sql .= ' ORDER BY ' . $order;
        }

        $query = $this->em->getConnection()->prepare($sql);
        foreach ($params as $key => $value) {
            $query->bindValue($key, $value);
        }

        SQLLoggerBroker::$loggerEnabled = FALSE;
        $query->execute();
        $data = $query->fetchAll();//var_dump($data);
        $count = count($data);


        if ($limit > 0) {
            $sql .= " LIMIT :limit OFFSET :offset";
            $params["limit"] = $limit;
            $params["offset"] = $start;
        }
        $query = $this->em->getConnection()->prepare($sql);
        foreach ($params as $key => $value) {
            $query->bindValue($key, $value);
        }
        if ($limit > 0) {
            $limit = intval($limit);
            $start = intval($start);
            $query->bindParam("limit", $limit, \PDO::PARAM_INT);
            $query->bindParam("offset", $start, \PDO::PARAM_INT);
        }

        $query->execute();
        $data = $query->fetchAll();
        SQLLoggerBroker::$loggerEnabled = TRUE;
        return ['data' => $data, 'count' => $count];
    }

    public function getDataCount()
    {
        $method = \callback($this->dataCountCallback);
        $sql = $method->invoke();
        $where = '';
        $params = [];
        foreach ($this->defaultFilters as $filter) {
            if (strlen($where) > 0) {
                $where .= ' AND ';
            }
            $where .= $filter['column'] . ' ' . $filter['operator'] . ' :p' . $filter['column'];
            $params['p' . $filter['column']] = $filter['value'];
        }
        if (strlen($where) > 0) {
            $sql .= ' WHERE ' . $where;
        }
        $query = $this->em->getConnection()->prepare($sql);
        foreach ($params as $key => $value) {
            $query->bindValue($key, $value);
        }
        $query->execute();
        $data = $query->fetch();
        return (int)$data["count"];
    }

    public function updateData($column, $id, $value)
    {
        // TODO: Implement updateData() method.
    }


    /**
     * @param \Nette\Utils\Callback $dataSourceCallback
     * @return DoctrineQBDataSource
     */
    public function setDataSourceCallback($dataSourceCallback)
    {
        $this->dataSourceCallback = $dataSourceCallback;
        return $this;
    }

    /**
     * @param \Nette\Utils\Callback $dataUpdateCallback
     * @return DoctrineQBDataSource
     */
    public function setDataUpdateCallback($dataUpdateCallback)
    {
        $this->dataUpdateCallback = $dataUpdateCallback;
        return $this;
    }

    /**
     * @param \Nette\Utils\Callback $dataCountCallback
     * @return DoctrineQBDataSource
     */
    public function setDataCountCallback($dataCountCallback)
    {
        $this->dataCountCallback = $dataCountCallback;
        return $this;
    }

    public function addDefaultFilter($column, $operator, $value)
    {
        $this->defaultFilters[] = ['column' => $column, 'operator' => $operator, 'value' => $value];
    }

    public function addDefaultOrder($column, $direction = 'ASC')
    {
        $this->defaultOrders[] = ['column' => $column, 'direction' => $direction];
    }


}