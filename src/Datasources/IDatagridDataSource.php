<?php
namespace Jija\Datagrid\Datasources;

interface IDatagridDataSource{
    public function getData($filters=[],$orderColumn = NULL, $orderDir = "ASC",$limit=null,$page=0);
    public function getDataCount();
    public function updateData($column, $id, $value);
}