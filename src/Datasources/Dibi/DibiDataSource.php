<?php
namespace Jija\Datagrid\Datasources\Dibi;


use Doctrine\ORM\EntityManager;

class DibiDataSource implements IDatagridDataSource
{

    protected $defaultFilters = [];

    protected $defaultOrders = [];

    public $model;



    public function getData($filters = [], $orderColumn = NULL, $orderDir = 'ASC', $limit = NULL, $start = 0)
    {
        $count = 0;
        if ($orderColumn != NULL) {
            $sort = [$orderColumn => $orderDir];
            $this->model->sort($sort);
        }
        $fils = [];
        foreach ($this->defaultFilters as $filter) {
            $fils[] = new FilterCondition($filter['column'], $filter['operator'] . ' ?', $filter['value']);

        }
        foreach ($filters as $col => $value) {
            $fils[] = new FilterCondition($col, 'LIKE ?', '%' . $value . '%');
        }

        $this->model->filter($fils);
        $count = count($this->model->getData());
        if ($limit != NULL) {
            $this->model->limit($start, $limit);
        }
        $data = $this->model->getData();
        foreach ($data as $key => $value) {
            $data[$key] = $value->toArray();
        }

        return ['data' => $data, 'count' => $count];
    }

    /**
     * Sets a model that implements the interface Grido\DataSources\IDataSource or data-source object.
     * @param mixed $model
     * @param bool $forceWrapper
     * @throws \InvalidArgumentException
     * @return Grid
     */
    public function setModel($model, $forceWrapper = FALSE)
    {
        $this->model = $model instanceof DataSourceWrapper && $forceWrapper === FALSE
            ? $model
            : new  DataSourceWrapper($model);

        return $this;
    }

    public function getDataCount()
    {
        return 0;
    }

    public function updateData($column, $id, $value)
    {
        return $value;
    }

    public function addDefaultFilter($column, $operator, $value)
    {
        $this->defaultFilters[] = ['column' => $column, 'operator' => $operator, 'value' => $value];
    }

    public function addDefaultOrder($column, $direction = 'ASC')
    {
        $this->defaultOrders[] = ['column' => $column, 'direction' => $direction];
    }


}