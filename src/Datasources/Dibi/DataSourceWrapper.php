<?php

namespace Jija\Datagrid\Datasources\Dibi;


class DataSourceWrapper extends \Nette\Object
{
    /** @var array */
    public $callback = array();

    /** @var DibiFluent */
    protected $dataSource;

    /**
     * @param mixed $model
     * @throws \InvalidArgumentException
     */
    public function __construct($model)
{
    if ($model instanceof \DibiFluent) {
        $dataSource = new DibiFluent($model);
    } else {
        throw new \InvalidArgumentException('Model must implement \Jija\Datagrid\DibiFluent.');
    }

    $this->dataSource = $dataSource;
}

    /**
     * @return DibiFluent
     */
    public function getDataSource()
{
    return $this->dataSource;
}

    public function __call($method, $args)
{
    return isset($this->callback[$method])
        ? callback($this->callback[$method])->invokeArgs(array($this->dataSource, $args))
        : call_user_func_array(array($this->dataSource, $method), $args);
}
}
