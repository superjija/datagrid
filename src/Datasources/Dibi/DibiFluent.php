<?php

namespace Jija\Datagrid\Datasources\Dibi;


class DibiFluent extends \Nette\Object
{
    /** @var \DibiFluent */
    protected $fluent;

    /** @var int */
    protected $limit;

    /** @var int */
    protected $offset;

    /**
     * @param \DibiFluent $fluent
     */
    public function __construct(\DibiFluent $fluent)
    {
        $this->fluent = $fluent;
    }

    /**
     * @return \DibiFluent
     */
    public function getFluent()
    {
        return $this->fluent;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param FilterCondition $condition
     * @param \DibiFluent $fluent
     */
    protected function makeWhere(FilterCondition $condition, \DibiFluent $fluent = NULL)
    {
        $fluent = $fluent === NULL
            ? $this->fluent
            : $fluent;

        if ($condition->callback) {
            callback($condition->callback)->invokeArgs(array($condition->value, $fluent));
        } else {
            call_user_func_array(array($fluent, 'where'), $condition->__toArray('[', ']'));
        }
    }

    /*********************************** interface IDataSource ************************************/

    /**
     * @return int
     */
    public function getCount()
    {
        $fluent = clone $this->fluent;
        return $fluent->count();
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->fluent->fetchAll($this->offset, $this->limit);
    }

    /**
     * @param array $conditions
     */
    public function filter(array $conditions)
    {
        foreach ($conditions as $condition) {
            $this->makeWhere($condition);
        }
    }

    /**
     * @param int $offset
     * @param int $limit
     */
    public function limit($offset, $limit)
    {
        $this->offset = $offset;
        $this->limit = $limit;
    }

    /**
     * @param array $sorting
     */
    public function sort(array $sorting)
    {
        foreach ($sorting as $column => $sort) {
            $this->fluent->orderBy("%n", $column, $sort);
        }
    }

    /**
     * @param mixed $column
     * @param array $conditions
     * @param int $limit
     * @return array
     */
    public function suggest($column, array $conditions, $limit)
    {
        $fluent = clone $this->fluent;
        is_string($column) && $fluent->removeClause('SELECT')->select("DISTINCT $column");

        foreach ($conditions as $condition) {
            $this->makeWhere($condition, $fluent);
        }

        $items = array();
        $data = $fluent->fetchAll(0, $limit);
        foreach ($data as $row) {
            if (is_string($column)) {
                $value = (string) $row[$column];
            } elseif (is_callable($column)) {
                $value = (string) $column($row);
            } else {
                $type = gettype($column);
                throw new \InvalidArgumentException("Column of suggestion must be string or callback, $type given.");
            }

            $items[$value] = $value;
        }

        return array_values($items);
    }
}
